#! /bin/bash

install_dir=./dist

#handle args
while [[ $1 != "" ]]; do
  case $1 in
    -i | --install-dir )
      shift
      install_dir=$1
      ;;
    * )
      echo "unrecognized token $1"
      exit
  esac
  shift
done

build_dir=$(pwd)/build
project_root=$(pwd)/src

echo going to install to: $install_dir

if [[ ! -d $build_dir ]]; then
  mkdir $build_dir
fi

pushd $build_dir
cmake $project_root -DCMAKE_INSTALL_PREFIX=$install_dir
popd
