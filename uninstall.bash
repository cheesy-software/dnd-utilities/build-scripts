#!/bin/bash

build_dir=./build
install_file=$build_dir/install_manifest.txt

if [[ ! -d $build_dir ]]; then
  echo "no build directory, don't know what to uninstall"
  exit
fi

if [[ ! -f $install_file ]]; then
  echo "no install_manifest, don't know what to uninstall"
  exit
fi

xargs rm < $install_file
