#! /bin/bash

build_dir=$(pwd)/build
global_install=0

#handle args
while [[ $1 != "" ]]; do
  case $1 in
    -g | --global-install )
      global_install=1
      ;;
    * )
      echo "unrecognized token $1"
      exit
  esac
  shift
done

#remove build dir for a clean build and install
if [[ -d $build_dir ]]; then
  rm -r $build_dir
fi

if [[ $global_install -eq 1 ]]; then
  ./build_scripts/config.bash --install-dir /usr/local
else
  ./build_scripts/config.bash
fi

pushd $build_dir
if [[ $global_install -eq 1 ]]; then
  sudo make -j 8 install
else
  make -j 8 install
fi
popd

